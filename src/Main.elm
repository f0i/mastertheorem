module Main exposing (..)

import Browser
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (main_)
import Html.Attributes as HtmlAttr
import List.Extra as Lst



-- MAIN


main : Program () Model Msg
main =
    Browser.sandbox { init = init, update = update, view = view }



-- MODEL


type alias Model =
    { a : String
    , b : String
    , fn : String
    , showDecimal : Bool
    }


init : Model
init =
    { a = "4"
    , b = "2"
    , fn = "2"
    , showDecimal = False
    }



-- UPDATE


type Msg
    = InputA String
    | InputB String
    | InputFn String
    | ShowDecimal Bool


update : Msg -> Model -> Model
update msg model =
    case msg of
        InputA val ->
            if String.toInt val /= Nothing || val == "" then
                { model | a = val }

            else
                model

        InputB val ->
            if String.toInt val /= Nothing || val == "" then
                { model | b = val }

            else
                model

        InputFn val ->
            if String.toInt val /= Nothing || val == "" then
                { model | fn = val }

            else
                model

        ShowDecimal show ->
            { model | showDecimal = show }


view model =
    layout [ height fill ] <|
        column [ spacing 30, centerX, paddingXY 20 30, height fill ]
            [ el [ centerX ] <| text "Master Theorem"
            , commands model
            , settings model
            , showCalculation model
            , link [ alignBottom, centerX, Font.size 10, Font.color <| rgb 0 0 0.5 ]
                { url = "https://f0i.de/impressum"
                , label = text "impressum"
                }
            ]


commands : Model -> Element Msg
commands model =
    wrappedRow
        [ spacing 10, centerX ]
        [ text "T(n) = "
        , Input.text [ width <| px 100 ]
            { onChange = InputA
            , text = model.a
            , placeholder = Just <| Input.placeholder [] <| text "2"
            , label = Input.labelRight [] (text "")
            }
        , text " * T(n / "
        , Input.text [ width <| px 100 ]
            { onChange = InputB
            , text = model.b
            , placeholder = Just <| Input.placeholder [] <| text "1"
            , label = Input.labelRight [] (text "")
            }
        , text ") + n ^"
        , Input.text [ width <| px 100 ]
            { onChange = InputFn
            , text = model.fn
            , placeholder = Just <| Input.placeholder [] <| text "1"
            , label = Input.labelRight [] (text "")
            }
        ]


settings : Model -> Element Msg
settings model =
    column [ spacing 10, centerX ]
        [ Input.checkbox []
            { onChange = ShowDecimal
            , icon = Input.defaultCheckbox
            , checked = model.showDecimal
            , label = Input.labelRight [] <| text "show decimal"
            }
        ]


btn : String -> Msg -> Element Msg
btn label msg =
    Input.button []
        { onPress = Just msg
        , label =
            el
                [ Border.width 1
                , mouseDown [ Background.color <| rgb 0.8 0.8 0.8 ]
                , paddingXY 5 10
                , Border.rounded 4
                ]
            <|
                text label
        }


iToS : Int -> String
iToS =
    String.fromInt


fToS : Float -> String
fToS =
    String.fromFloat


takeRight : Int -> List a -> List a
takeRight n list =
    list |> List.reverse |> List.take n |> List.reverse


dropRight : Int -> List a -> List a
dropRight n list =
    list |> List.reverse |> List.drop n |> List.reverse


isEven : Int -> Bool
isEven n =
    n |> modBy 2 |> (==) 0


showCalculation : Model -> Element msg
showCalculation model =
    let
        a =
            model.a |> String.toInt |> Maybe.withDefault 1

        b =
            model.b |> String.toInt |> Maybe.withDefault 1

        fn =
            model.fn |> String.toInt |> Maybe.withDefault 1

        logba =
            logBase (toFloat b) (toFloat a)

        logbaFloor =
            floor logba

        logbaStr =
            if logba - toFloat logbaFloor == 0 then
                iToS logbaFloor

            else if model.showDecimal then
                fToS logba

            else
                iToS logbaFloor ++ ".X"

        nlogabStr =
            "log_" ++ iToS b ++ "(" ++ iToS a ++ ")"

        aStr =
            case a of
                1 ->
                    ""

                i ->
                    iToS a ++ " * "

        fnStr =
            case fn of
                0 ->
                    "1"

                1 ->
                    "n"

                i ->
                    "n^" ++ iToS i
    in
    column [ spacing 10, Font.family [ Font.monospace ] ] <|
        [ text <| "T(n) = " ++ aStr ++ "T(n / " ++ iToS b ++ ") + " ++ fnStr
        , text <| "       a = " ++ iToS a
        , text <| "              b = " ++ iToS b
        , text <| "                      f(n) = " ++ fnStr
        , text <| nlogabStr ++ " = " ++ logbaStr
        , if logba > toFloat fn then
            text <|
                ("n^" ++ logbaStr ++ " > " ++ fnStr ++ " => case 1\n")
                    ++ ("T(n) = O(n^" ++ logbaStr ++ ")")

          else if logba == toFloat fn then
            text <|
                ("n^" ++ logbaStr ++ " = " ++ fnStr ++ " => case 2\n")
                    ++ ("T(n) = O(" ++ fnStr ++ " * log(n)" ++ ")")

          else
            text <|
                ("n^" ++ logbaStr ++ " < " ++ fnStr ++ " => case 3\n")
                    ++ ("T(n) = O(" ++ fnStr ++ ")")
        ]
